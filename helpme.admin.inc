<?php

function helpme_overview() {
  $sql = 'SELECT * FROM {helpme}';
  
  $header = array(
    array('data' => t('Path'), 'field' => 'path'),
    array('data' => t('Operations'), 'colspan' => '2')
  );
  $sql .= tablesort_sql($header);
  $result = pager_query($sql, 50, 0 , NULL, NULL);

  $rows = array();
  $destination = drupal_get_destination();
  while ($data = db_fetch_object($result)) {
    $row = array(
      check_plain($data->path),
      l(t('edit'), "admin/build/helpme/edit/$data->hid", array('query' => $destination)),
      l(t('delete'), "admin/build/helpme/delete/$data->hid", array('query' => $destination))
    );
    $rows[] = $row;
  }

  if (empty($rows)) {
    $empty_message = t('No help records found.');
    $rows[] = array(array('data' => $empty_message, 'colspan' => ''));
  }

  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);


  return $output;
}

function helpme_admin_form(&$form_state, $edit = array('help' => '', 'path' => '', 'hid' => NULL)) {
  $form['#helpme'] = $edit;

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $edit['path'],
    '#maxlength' => 128,
    '#size' => 45,
    '#description' => t('Specify a path by which this help message will be displayed.'),
    '#required' => TRUE,
  );
  $form['help'] = array(
    '#type' => 'textarea',
    '#title' => t('Help message'),
    '#default_value' => $edit['help'],
    '#description' => t('Specify a help message for this path.'),
    '#required' => TRUE,
  );
  if ($edit['hid']) {
    $form['hid'] = array('#type' => 'hidden', '#value' => $edit['hid']);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update help message'));
  }
  else {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Create new help message'));
  }

  return $form;
}

function helpme_admin_form_submit($form, &$form_state) {
  helpme_set_shortcut($form_state['values']['help'], $form_state['values']['path'], isset($form_state['values']['hid']) ? $form_state['values']['hid'] : 0);

  drupal_set_message(t('The help message has been saved.'));
  $form_state['redirect'] = 'admin/build/helpme/add';
  return;

}

function helpme_admin_edit($hid = 0) {
  if ($hid) {
    $helpme = helpme_load($hid);
    drupal_set_title(t('Edit help message for path "!path"', array('!path' => check_plain($helpme['path']))));
    $output = drupal_get_form('helpme_admin_form', $helpme);
  }
  else {
    $output = drupal_get_form('helpme_admin_form');
  }

  return $output;
}

function helpme_load($hid) {
  return db_fetch_array(db_query('SELECT * FROM {helpme} WHERE hid = %d', $hid));
}

function helpme_set_shortcut($help = NULL, $path = NULL, $hid = NULL) {
  $help = urldecode($help);
  $path = urldecode($path);
  // First we check if we deal with an existing help message and delete or modify it based on hid.
  if ($hid) {
    // An existing alias.
    if (!$help || !$path) {
      // Delete the help msg based on hid.
      db_query('DELETE FROM {helpme} WHERE hid = %d', $hid);
    }
    else {
      // Update the existing help message.
      db_query("UPDATE {helpme} SET help = '%s', path = '%s' WHERE hid = %d", $help, $path, $hid);
    }
  }
  else if ($help && $path) {
      // A new help message. Add it to the database.
    db_query("INSERT INTO {helpme} (help, path) VALUES ('%s', '%s')", $help, $path);
  }
  else {
    // Delete the help message.
    if ($path) {
      db_query("DELETE FROM {helpme} WHERE help = '%s'", $help);
    }
    else {
      db_query("DELETE FROM {helpme} WHERE path = '%s'", $path);
    }
  }
  drupal_clear_path_cache();
}

function helpme_admin_delete_confirm($form_state, $sid) {
  $helpme = helpme_load($hid);
  if (user_access('administer helpme')) {
    $form['hid'] = array('#type' => 'value', '#value' => $hid);
    $output = confirm_form($form,
      t('Are you sure you want to delete the help message for path %path?', array('%title' => $helpme['path'])),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/build/helpme');
  }
  return $output;
}

function helpme_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    helpme_admin_delete($form_state['values']['hid']);
    $form_state['redirect'] = 'admin/build/helpme';
    return;
  }
}

function helpme_admin_delete($sid = 0) {
  db_query('DELETE FROM {helpme} WHERE hid = %d', $hid);
  drupal_set_message(t('The help message has been deleted.'));
}
